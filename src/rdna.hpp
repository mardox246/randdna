#include <iostream>
#include <string>
#include <random>

using namespace std;

string randDNA(int s, string b, int n)
{
	
	mt19937 eng1(s);
	
	int min = 0;
	int max = b.size()-1;
	uniform_int_distribution<>uform(min,max);
	int index = 0;
	string ATCG = "";
	for(int i = 0; i < n; i++)
	{
		index = uform(eng1);
		ATCG += b[index];
	 }

return ATCG;
}
